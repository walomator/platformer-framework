extends Node2D

var root_node

enum {TOP, BOTTOM, LEFT, RIGHT}

func _ready():
	root_node = get_node("/root/")
	
	root_node.connect("size_changed", self, "handle_size_changed")
	

func handle_size_changed():
	pass


func handle_shutdown():
	print("Shutting down")
	get_tree().quit()
	

func camera_move(offset_vector): # DEV - Advanced camera currently in use
	var canvas_transform = get_viewport().get_canvas_transform()
	canvas_transform += offset_vector
	get_viewport().set_canvas_transform(-canvas_transform)
	

func handle_exited_center_box(direction): # DEV - Advanced camera not currently in use
	camera_move(direction)
